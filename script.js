$(".btn-page").fadeOut(); // jQuery метод .fadeOut() позволяет плавно изменить прозрачность для отдельных элементов (из видимого состояния в скрытое).По завершению  display установлено в значение none. Может быть без параметров и может быть с параметрами.  Скрываю .btn-page - кнопку "Наверх"

$(".page1").click(function () {
  slideTitle(".most-posts", 35);
});

$(".page2").click(function () {
  slideTitle(".our-clients", 25);
});

$(".page3").click(function () {
  slideTitle(".btn-toggle-top-rated", 280);
});

$(".page4").click(function () {
  slideTitle(".hot-news", 35);
});

// jQuery метод .animate() позволяет выполнить пользовательскую анимацию, основанную на изменении CSS свойств для выбранных элементов - $( selector ).animate( { properties }, { options } ). scrollTop устанавливает количество пикселей прокрученных от верха элемента, offsetTop содержит расстояние от offsetParent до границы элемента. 1000 - продолжительность анимации миллисекунды.
function slideTitle(className, level) {
  $("html, body").animate(
    { scrollTop: ($(`${className}`).offset().top -= `${level}`) },
    1000
  );
}

// открытие кнопки "Наверх" в случае скролинга 650px и больше, до 650 будет скрыта
$(window).scroll(function (event) {
  let top = $(window).scrollTop();
  if (top >= 650) {
    $(".btn-page").fadeIn();
  } else {
    $(".btn-page").fadeOut();
  }
});

// нажатие на кнопку "Наверх" прокрутка страницы в самый верх, на протяжении 1000мс
$(".btn-page").click(function () {
  $("body,html").animate(
    {
      scrollTop: 0,
    },
    1000
  );
  return false;
});

// Скрытие и открытие секции 'Top Rated'
$(".btn-toggle-top-rated").click(function () {              // jQuery метод .click()   
     // jQuery метод .slideToggle() позволяет плавно скрыть или отобразить выбранные элементы (.top-rated и .content-rated). Метод производит анимацию только высоты. Параметр "slow" (строковое или числовое значение) соотвествует 600 миллисекундам, по умолчанию 400 миллисекунд
  $(".top-rated, .content-rated").slideToggle("slow");   
  //  jQuery метод .text() задает или возвращает текстовое содержимое выбранных элементов. Смена надписи кнопки при клике
  if ($(this).text() == "открыть Top Rated") {
    $(this).text("скрыть 'Top Rated");
  } else {
    $(this).text("открыть Top Rated");
  }
});
